import cartotool
from cartotool import Configuration, Pageable, ProjectForm


print("creating project")

token = "ff37bbc3-a2fe-4584-8f25-4d70a2791790"

configuration = cartotool.Configuration(
    host="http://localhost:8080",
    api_key={'Authorization': token},
    api_key_prefix={'Authorization': "TOKEN"},
)

cartotool.Configuration.set_default(configuration)

api_client = cartotool.openapi_client.ApiClient()

me = cartotool.openapi_client.UserControllerApi().find_me()
print(me.name)

workspaces = cartotool.openapi_client.WorkspaceControllerApi().get_all(pageable=Pageable(page=0))
print(workspaces[0]['code'])

project = cartotool.openapi_client.ProjectControllerApi().get_projects(pageable=Pageable(page=0))
print(project[0].code)

new_project = cartotool.openapi_client.ProjectControllerApi().post_projects(project_form=ProjectForm(
    workspace_code="3b68befa-38e2-4396-89fd-12d2b5f1d5f3",
    name="test Project",
))

print(new_project.code)