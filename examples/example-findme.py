from cartotool import UserControllerApi, ApiClient, Configuration

token = "7451f8ed-8126-4498-8763-5a5cc03088de"

configuration = Configuration(
    host="https://test.cartotool.com",
    api_key={'Authorization': token},
    api_key_prefix={'Authorization': "TOKEN"},
)

client = ApiClient(configuration)
me = UserControllerApi(client).find_me()

print(me)