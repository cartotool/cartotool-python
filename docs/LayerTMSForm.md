# LayerTMSForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**footnote** | **str** |  | [optional] 
**group** | **int** |  | [optional] 
**position** | **int** |  | [optional] 
**project** | **str** |  | [optional] 
**url** | **str** |  | [optional] 
**layer_parameter** | **str** |  | [optional] 
**layer_style** | **str** |  | [optional] 
**layer_zoom** | **int** |  | [optional] 
**legend_enabled** | **bool** |  | [optional] 
**polygon_styles** | [**list[LayerStylePolygon]**](LayerStylePolygon.md) |  | [optional] 
**point_styles** | [**list[LayerStylePoint]**](LayerStylePoint.md) |  | [optional] 
**hidden_layer** | **bool** |  | [optional] 
**visible** | **bool** |  | [optional] 
**opacity** | **float** |  | [optional] 
**tile_size** | **int** |  | [optional] 
**min_zoom** | **int** |  | [optional] 
**max_zoom** | **int** |  | [optional] 
**projection** | **str** |  | [optional] 
**local** | **bool** |  | [optional] 
**base_layer** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


