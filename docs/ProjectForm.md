# ProjectForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**workspace_code** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**watermark** | **str** |  | [optional] 
**image** | **str** |  | [optional] 
**longitude** | **float** |  | [optional] 
**latitude** | **float** |  | [optional] 
**zoom** | **int** |  | [optional] 
**base_url** | **str** |  | [optional] 
**public** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


