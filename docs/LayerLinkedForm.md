# LayerLinkedForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**project** | **str** |  | [optional] 
**layer** | **str** |  | [optional] 
**group** | **int** |  | [optional] 
**hidden_layer** | **bool** |  | [optional] 
**base_layer** | **bool** |  | [optional] 
**visible** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


