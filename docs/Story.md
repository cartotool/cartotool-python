# Story

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**project_code** | **str** |  | [optional] 
**id** | **int** |  | [optional] 
**code** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**markdown** | **str** |  | [optional] 
**image** | **str** |  | [optional] 
**creation_date** | **datetime** |  | [optional] 
**project** | [**Project**](Project.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


