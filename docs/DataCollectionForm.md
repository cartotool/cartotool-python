# DataCollectionForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**project** | **str** |  | [optional] 
**size** | **int** |  | [optional] 
**head** | **list[str]** |  | [optional] 
**geometry_srid** | **int** |  | [optional] 
**geometry_type** | **str** |  | [optional] 
**geometry_cols** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


