# Status

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authorities** | **list[str]** |  | [optional] 
**logged_in** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


