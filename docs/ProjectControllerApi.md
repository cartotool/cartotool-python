# cartotool.ProjectControllerApi

All URIs are relative to *http://cartotool.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_project**](ProjectControllerApi.md#delete_project) | **DELETE** /api/projects/{projectCode} | 
[**get_project**](ProjectControllerApi.md#get_project) | **GET** /api/projects/{projectCode} | 
[**get_projects**](ProjectControllerApi.md#get_projects) | **GET** /api/projects | 
[**post_projects**](ProjectControllerApi.md#post_projects) | **POST** /api/projects | 
[**update_project**](ProjectControllerApi.md#update_project) | **PUT** /api/projects/{projectCode} | 


# **delete_project**
> Project delete_project(project_code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.ProjectControllerApi(api_client)
    project_code = 'project_code_example' # str | 

    try:
        api_response = api_instance.delete_project(project_code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ProjectControllerApi->delete_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_code** | **str**|  | 

### Return type

[**Project**](Project.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_project**
> Project get_project(project_code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.ProjectControllerApi(api_client)
    project_code = 'project_code_example' # str | 

    try:
        api_response = api_instance.get_project(project_code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ProjectControllerApi->get_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_code** | **str**|  | 

### Return type

[**Project**](Project.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_projects**
> list[Project] get_projects(pageable, search=search)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.ProjectControllerApi(api_client)
    pageable = cartotool.Pageable() # Pageable | 
search = 'search_example' # str |  (optional)

    try:
        api_response = api_instance.get_projects(pageable, search=search)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ProjectControllerApi->get_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pageable** | [**Pageable**](.md)|  | 
 **search** | **str**|  | [optional] 

### Return type

[**list[Project]**](Project.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_projects**
> Project post_projects(project_form)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.ProjectControllerApi(api_client)
    project_form = cartotool.ProjectForm() # ProjectForm | 

    try:
        api_response = api_instance.post_projects(project_form)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ProjectControllerApi->post_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_form** | [**ProjectForm**](ProjectForm.md)|  | 

### Return type

[**Project**](Project.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_project**
> Project update_project(project_code, project_form)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.ProjectControllerApi(api_client)
    project_code = 'project_code_example' # str | 
project_form = cartotool.ProjectForm() # ProjectForm | 

    try:
        api_response = api_instance.update_project(project_code, project_form)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ProjectControllerApi->update_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_code** | **str**|  | 
 **project_form** | [**ProjectForm**](ProjectForm.md)|  | 

### Return type

[**Project**](Project.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

