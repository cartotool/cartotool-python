# Project

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**code** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**watermark** | **str** |  | [optional] 
**workspace** | [**Workspace**](Workspace.md) |  | [optional] 
**image** | [**ProjectImage**](ProjectImage.md) |  | [optional] 
**creation_date** | **datetime** |  | [optional] 
**longitude** | **float** |  | [optional] 
**latitude** | **float** |  | [optional] 
**zoom** | **int** |  | [optional] 
**base_url** | **str** |  | [optional] 
**public** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


