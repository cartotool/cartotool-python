# LayerVectorForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**footnote** | **str** |  | [optional] 
**collection** | **str** |  | [optional] 
**col** | **int** |  | [optional] 
**group** | **int** |  | [optional] 
**position** | **int** |  | [optional] 
**polygon_styles** | [**list[LayerStylePolygon]**](LayerStylePolygon.md) |  | [optional] 
**point_styles** | [**list[LayerStylePoint]**](LayerStylePoint.md) |  | [optional] 
**dashboard** | [**LayerDashboard**](LayerDashboard.md) |  | [optional] 
**project** | **str** |  | [optional] 
**on_hover_enabled** | **bool** |  | [optional] 
**legend_enabled** | **bool** |  | [optional] 
**discrete_color_enabled** | **bool** |  | [optional] 
**dashboard_enabled** | **bool** |  | [optional] 
**on_hover_color** | **str** |  | [optional] 
**categorical** | **bool** |  | [optional] 
**hidden_layer** | **bool** |  | [optional] 
**visible** | **bool** |  | [optional] 
**opacity** | **float** |  | [optional] 
**base_layer** | **bool** |  | [optional] 
**point_clustering_enabled** | **bool** |  | [optional] 
**point_clustering_radius** | **float** |  | [optional] 
**tooltip_enabled** | **bool** |  | [optional] 
**tooltip_col** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


