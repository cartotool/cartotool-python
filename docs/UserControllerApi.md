# cartotool.UserControllerApi

All URIs are relative to *http://cartotool.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_user**](UserControllerApi.md#create_user) | **POST** /api/users | 
[**delete_user**](UserControllerApi.md#delete_user) | **DELETE** /api/users/{code} | 
[**find_all_users**](UserControllerApi.md#find_all_users) | **GET** /api/users | 
[**find_all_users_by_codes**](UserControllerApi.md#find_all_users_by_codes) | **POST** /api/users/search | 
[**find_me_user**](UserControllerApi.md#find_me_user) | **GET** /api/users/me | 
[**find_me_user_accounts**](UserControllerApi.md#find_me_user_accounts) | **GET** /api/users/me/accounts | 
[**find_user_by_id**](UserControllerApi.md#find_user_by_id) | **GET** /api/users/{code} | 
[**generate_user_reset_code**](UserControllerApi.md#generate_user_reset_code) | **PUT** /api/users/reset-password | 
[**generate_user_reset_code_for_user_code**](UserControllerApi.md#generate_user_reset_code_for_user_code) | **PUT** /api/users/{code}/reset-password | 
[**update_user**](UserControllerApi.md#update_user) | **PUT** /api/users/{code} | 


# **create_user**
> User create_user(user_form)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.UserControllerApi(api_client)
    user_form = cartotool.UserForm() # UserForm | 

    try:
        api_response = api_instance.create_user(user_form)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UserControllerApi->create_user: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_form** | [**UserForm**](UserForm.md)|  | 

### Return type

[**User**](User.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_user**
> object delete_user(code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.UserControllerApi(api_client)
    code = 'code_example' # str | 

    try:
        api_response = api_instance.delete_user(code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UserControllerApi->delete_user: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

**object**

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_all_users**
> list[User] find_all_users(page, search=search)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.UserControllerApi(api_client)
    page = cartotool.Pageable() # Pageable | 
search = '' # str |  (optional) (default to '')

    try:
        api_response = api_instance.find_all_users(page, search=search)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UserControllerApi->find_all_users: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | [**Pageable**](.md)|  | 
 **search** | **str**|  | [optional] [default to &#39;&#39;]

### Return type

[**list[User]**](User.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_all_users_by_codes**
> list[User] find_all_users_by_codes(request_body)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.UserControllerApi(api_client)
    request_body = ['request_body_example'] # list[str] | 

    try:
        api_response = api_instance.find_all_users_by_codes(request_body)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UserControllerApi->find_all_users_by_codes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body** | [**list[str]**](str.md)|  | 

### Return type

[**list[User]**](User.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_me_user**
> User find_me_user()



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.UserControllerApi(api_client)
    
    try:
        api_response = api_instance.find_me_user()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UserControllerApi->find_me_user: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**User**](User.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_me_user_accounts**
> list[UserAccount] find_me_user_accounts()



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.UserControllerApi(api_client)
    
    try:
        api_response = api_instance.find_me_user_accounts()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UserControllerApi->find_me_user_accounts: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[UserAccount]**](UserAccount.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_user_by_id**
> User find_user_by_id(code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.UserControllerApi(api_client)
    code = 'code_example' # str | 

    try:
        api_response = api_instance.find_user_by_id(code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UserControllerApi->find_user_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

[**User**](User.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **generate_user_reset_code**
> object generate_user_reset_code(request_password_reset)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.UserControllerApi(api_client)
    request_password_reset = cartotool.RequestPasswordReset() # RequestPasswordReset | 

    try:
        api_response = api_instance.generate_user_reset_code(request_password_reset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UserControllerApi->generate_user_reset_code: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_password_reset** | [**RequestPasswordReset**](RequestPasswordReset.md)|  | 

### Return type

**object**

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **generate_user_reset_code_for_user_code**
> object generate_user_reset_code_for_user_code(code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.UserControllerApi(api_client)
    code = 'code_example' # str | 

    try:
        api_response = api_instance.generate_user_reset_code_for_user_code(code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UserControllerApi->generate_user_reset_code_for_user_code: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

**object**

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_user**
> User update_user(code, user_form)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.UserControllerApi(api_client)
    code = 'code_example' # str | 
user_form = cartotool.UserForm() # UserForm | 

    try:
        api_response = api_instance.update_user(code, user_form)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UserControllerApi->update_user: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 
 **user_form** | [**UserForm**](UserForm.md)|  | 

### Return type

[**User**](User.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

