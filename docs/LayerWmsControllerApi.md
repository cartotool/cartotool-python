# cartotool.LayerWmsControllerApi

All URIs are relative to *http://cartotool.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**post_layer_wms**](LayerWmsControllerApi.md#post_layer_wms) | **POST** /api/layers-wms | 
[**put_layer_wms**](LayerWmsControllerApi.md#put_layer_wms) | **PUT** /api/layers-wms/{code} | 


# **post_layer_wms**
> Layer post_layer_wms(layer_wms_form)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.LayerWmsControllerApi(api_client)
    layer_wms_form = cartotool.LayerWmsForm() # LayerWmsForm | 

    try:
        api_response = api_instance.post_layer_wms(layer_wms_form)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LayerWmsControllerApi->post_layer_wms: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **layer_wms_form** | [**LayerWmsForm**](LayerWmsForm.md)|  | 

### Return type

[**Layer**](Layer.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_layer_wms**
> Layer put_layer_wms(code, layer_wms_form)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.LayerWmsControllerApi(api_client)
    code = 'code_example' # str | 
layer_wms_form = cartotool.LayerWmsForm() # LayerWmsForm | 

    try:
        api_response = api_instance.put_layer_wms(code, layer_wms_form)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LayerWmsControllerApi->put_layer_wms: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 
 **layer_wms_form** | [**LayerWmsForm**](LayerWmsForm.md)|  | 

### Return type

[**Layer**](Layer.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

