# cartotool.DataCollectionControllerApi

All URIs are relative to *http://cartotool.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_data_collection_collection**](DataCollectionControllerApi.md#create_data_collection_collection) | **POST** /api/collections | 
[**delete_data_collection_by_code**](DataCollectionControllerApi.md#delete_data_collection_by_code) | **DELETE** /api/collections/{code} | 
[**find_all_data_collections**](DataCollectionControllerApi.md#find_all_data_collections) | **GET** /api/collections | 
[**find_data_collection_by_code**](DataCollectionControllerApi.md#find_data_collection_by_code) | **GET** /api/collections/{code} | 
[**find_data_collection_layers_by_code**](DataCollectionControllerApi.md#find_data_collection_layers_by_code) | **GET** /api/collections/{code}/layers | 


# **create_data_collection_collection**
> DataCollection create_data_collection_collection(data_collection_form)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.DataCollectionControllerApi(api_client)
    data_collection_form = cartotool.DataCollectionForm() # DataCollectionForm | 

    try:
        api_response = api_instance.create_data_collection_collection(data_collection_form)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DataCollectionControllerApi->create_data_collection_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data_collection_form** | [**DataCollectionForm**](DataCollectionForm.md)|  | 

### Return type

[**DataCollection**](DataCollection.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_data_collection_by_code**
> object delete_data_collection_by_code(code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.DataCollectionControllerApi(api_client)
    code = 'code_example' # str | 

    try:
        api_response = api_instance.delete_data_collection_by_code(code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DataCollectionControllerApi->delete_data_collection_by_code: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

**object**

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_all_data_collections**
> list[DataCollection] find_all_data_collections(project, page, status=status)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.DataCollectionControllerApi(api_client)
    project = 'project_example' # str | 
page = cartotool.Pageable() # Pageable | 
status = ['status_example'] # list[str] |  (optional)

    try:
        api_response = api_instance.find_all_data_collections(project, page, status=status)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DataCollectionControllerApi->find_all_data_collections: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**|  | 
 **page** | [**Pageable**](.md)|  | 
 **status** | [**list[str]**](str.md)|  | [optional] 

### Return type

[**list[DataCollection]**](DataCollection.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_data_collection_by_code**
> DataCollection find_data_collection_by_code(code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.DataCollectionControllerApi(api_client)
    code = 'code_example' # str | 

    try:
        api_response = api_instance.find_data_collection_by_code(code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DataCollectionControllerApi->find_data_collection_by_code: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

[**DataCollection**](DataCollection.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_data_collection_layers_by_code**
> list[Layer] find_data_collection_layers_by_code(code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.DataCollectionControllerApi(api_client)
    code = 'code_example' # str | 

    try:
        api_response = api_instance.find_data_collection_layers_by_code(code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DataCollectionControllerApi->find_data_collection_layers_by_code: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

[**list[Layer]**](Layer.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

