# cartotool.LayerVectorControllerApi

All URIs are relative to *http://cartotool.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**post_layer_vector**](LayerVectorControllerApi.md#post_layer_vector) | **POST** /api/layers-vector | 
[**put_layer_vector**](LayerVectorControllerApi.md#put_layer_vector) | **PUT** /api/layers-vector/{code} | 


# **post_layer_vector**
> Layer post_layer_vector(layer_vector_form)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.LayerVectorControllerApi(api_client)
    layer_vector_form = cartotool.LayerVectorForm() # LayerVectorForm | 

    try:
        api_response = api_instance.post_layer_vector(layer_vector_form)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LayerVectorControllerApi->post_layer_vector: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **layer_vector_form** | [**LayerVectorForm**](LayerVectorForm.md)|  | 

### Return type

[**Layer**](Layer.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_layer_vector**
> Layer put_layer_vector(code, layer_vector_form)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.LayerVectorControllerApi(api_client)
    code = 'code_example' # str | 
layer_vector_form = cartotool.LayerVectorForm() # LayerVectorForm | 

    try:
        api_response = api_instance.put_layer_vector(code, layer_vector_form)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LayerVectorControllerApi->put_layer_vector: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 
 **layer_vector_form** | [**LayerVectorForm**](LayerVectorForm.md)|  | 

### Return type

[**Layer**](Layer.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

