# LayerStylePolygon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**value** | **str** |  | [optional] 
**label** | **str** |  | [optional] 
**position** | **int** |  | [optional] 
**fill_color** | **str** |  | [optional] 
**stroke_color** | **str** |  | [optional] 
**stroke_width** | **float** |  | [optional] 
**stroke_line_dash** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


