# cartotool.WorkspaceControllerApi

All URIs are relative to *http://cartotool.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_workspace**](WorkspaceControllerApi.md#delete_workspace) | **DELETE** /api/workspaces/{code} | 
[**delete_workspace_user**](WorkspaceControllerApi.md#delete_workspace_user) | **DELETE** /api/workspaces/{workspaceCode}/users/{userCode} | 
[**get_all_workspaces**](WorkspaceControllerApi.md#get_all_workspaces) | **GET** /api/workspaces | 
[**get_workspace**](WorkspaceControllerApi.md#get_workspace) | **GET** /api/workspaces/{code} | 
[**get_workspace_projects**](WorkspaceControllerApi.md#get_workspace_projects) | **GET** /api/workspaces/{code}/projects | 
[**get_workspace_users**](WorkspaceControllerApi.md#get_workspace_users) | **GET** /api/workspaces/{code}/users | 
[**post_workspace**](WorkspaceControllerApi.md#post_workspace) | **POST** /api/workspaces | 
[**post_workspace_user**](WorkspaceControllerApi.md#post_workspace_user) | **POST** /api/workspaces/{workspaceCode}/users | 


# **delete_workspace**
> object delete_workspace(code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.WorkspaceControllerApi(api_client)
    code = 'code_example' # str | 

    try:
        api_response = api_instance.delete_workspace(code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkspaceControllerApi->delete_workspace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

**object**

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_workspace_user**
> Workspace delete_workspace_user(workspace_code, user_code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.WorkspaceControllerApi(api_client)
    workspace_code = 'workspace_code_example' # str | 
user_code = 'user_code_example' # str | 

    try:
        api_response = api_instance.delete_workspace_user(workspace_code, user_code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkspaceControllerApi->delete_workspace_user: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspace_code** | **str**|  | 
 **user_code** | **str**|  | 

### Return type

[**Workspace**](Workspace.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_workspaces**
> object get_all_workspaces(pageable)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.WorkspaceControllerApi(api_client)
    pageable = cartotool.Pageable() # Pageable | 

    try:
        api_response = api_instance.get_all_workspaces(pageable)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkspaceControllerApi->get_all_workspaces: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pageable** | [**Pageable**](.md)|  | 

### Return type

**object**

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_workspace**
> Workspace get_workspace(code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.WorkspaceControllerApi(api_client)
    code = 'code_example' # str | 

    try:
        api_response = api_instance.get_workspace(code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkspaceControllerApi->get_workspace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

[**Workspace**](Workspace.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_workspace_projects**
> list[Project] get_workspace_projects(code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.WorkspaceControllerApi(api_client)
    code = 'code_example' # str | 

    try:
        api_response = api_instance.get_workspace_projects(code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkspaceControllerApi->get_workspace_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

[**list[Project]**](Project.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_workspace_users**
> list[User] get_workspace_users(code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.WorkspaceControllerApi(api_client)
    code = 'code_example' # str | 

    try:
        api_response = api_instance.get_workspace_users(code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkspaceControllerApi->get_workspace_users: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

[**list[User]**](User.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_workspace**
> Workspace post_workspace(workspace_form)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.WorkspaceControllerApi(api_client)
    workspace_form = cartotool.WorkspaceForm() # WorkspaceForm | 

    try:
        api_response = api_instance.post_workspace(workspace_form)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkspaceControllerApi->post_workspace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspace_form** | [**WorkspaceForm**](WorkspaceForm.md)|  | 

### Return type

[**Workspace**](Workspace.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_workspace_user**
> Workspace post_workspace_user(workspace_code, workspace_user_form)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.WorkspaceControllerApi(api_client)
    workspace_code = 'workspace_code_example' # str | 
workspace_user_form = cartotool.WorkspaceUserForm() # WorkspaceUserForm | 

    try:
        api_response = api_instance.post_workspace_user(workspace_code, workspace_user_form)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkspaceControllerApi->post_workspace_user: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspace_code** | **str**|  | 
 **workspace_user_form** | [**WorkspaceUserForm**](WorkspaceUserForm.md)|  | 

### Return type

[**Workspace**](Workspace.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

