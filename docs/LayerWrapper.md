# LayerWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**layer** | [**Layer**](Layer.md) |  | [optional] 
**linked** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


