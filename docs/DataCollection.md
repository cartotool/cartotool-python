# DataCollection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**code** | **str** |  | [optional] 
**project** | [**Project**](Project.md) |  | [optional] 
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**size** | **int** |  | [optional] 
**head** | **list[str]** |  | [optional] 
**geometry** | [**DataCollectionGeometry**](DataCollectionGeometry.md) |  | [optional] 
**status** | **str** |  | [optional] 
**creation_date** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


