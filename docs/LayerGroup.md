# LayerGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**parent** | [**LayerGroup**](LayerGroup.md) |  | [optional] 
**position** | **int** |  | [optional] 
**project** | [**Project**](Project.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


