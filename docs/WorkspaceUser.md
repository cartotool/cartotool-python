# WorkspaceUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_code** | **str** |  | [optional] 
**id** | **int** |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**role** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


