# LayerBitmapForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**footnote** | **str** |  | [optional] 
**group** | **int** |  | [optional] 
**position** | **int** |  | [optional] 
**project** | **str** |  | [optional] 
**polygon_styles** | [**list[LayerStylePolygon]**](LayerStylePolygon.md) |  | [optional] 
**point_styles** | [**list[LayerStylePoint]**](LayerStylePoint.md) |  | [optional] 
**legend_enabled** | **bool** |  | [optional] 
**hidden_layer** | **bool** |  | [optional] 
**visible** | **bool** |  | [optional] 
**opacity** | **float** |  | [optional] 
**base_layer** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


