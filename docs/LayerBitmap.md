# LayerBitmap

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**code** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**footnote** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**position** | **int** |  | [optional] 
**visible** | **bool** |  | [optional] 
**opacity** | **float** |  | [optional] 
**hidden_layer** | **bool** |  | [optional] 
**legend_enabled** | **bool** |  | [optional] 
**base_layer** | **bool** |  | [optional] 
**group** | [**LayerGroup**](LayerGroup.md) |  | [optional] 
**project** | [**Project**](Project.md) |  | [optional] 
**polygon_styles** | [**list[LayerStylePolygon]**](LayerStylePolygon.md) |  | [optional] 
**point_styles** | [**list[LayerStylePoint]**](LayerStylePoint.md) |  | [optional] 
**upload_id** | **str** |  | [optional] 
**z_index** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


