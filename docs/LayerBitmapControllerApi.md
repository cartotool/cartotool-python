# cartotool.LayerBitmapControllerApi

All URIs are relative to *http://cartotool.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_layer_bitmap_tiles**](LayerBitmapControllerApi.md#get_layer_bitmap_tiles) | **GET** /api/layers-bitmap/{code}/tiles/{z}/{x}/{y} | 
[**post_layer_bitmap**](LayerBitmapControllerApi.md#post_layer_bitmap) | **POST** /api/layers-bitmap | 
[**put_layer_bitmap**](LayerBitmapControllerApi.md#put_layer_bitmap) | **PUT** /api/layers-bitmap/{code} | 
[**upload_layer_bitmap_chunk**](LayerBitmapControllerApi.md#upload_layer_bitmap_chunk) | **POST** /api/layers-bitmap/{code}/chunk | 
[**upload_layer_bitmap_complete**](LayerBitmapControllerApi.md#upload_layer_bitmap_complete) | **POST** /api/layers-bitmap/{code}/complete | 


# **get_layer_bitmap_tiles**
> list[str] get_layer_bitmap_tiles(code, x, y, z)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.LayerBitmapControllerApi(api_client)
    code = 'code_example' # str | 
x = 56 # int | 
y = 56 # int | 
z = 56 # int | 

    try:
        api_response = api_instance.get_layer_bitmap_tiles(code, x, y, z)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LayerBitmapControllerApi->get_layer_bitmap_tiles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 
 **x** | **int**|  | 
 **y** | **int**|  | 
 **z** | **int**|  | 

### Return type

**list[str]**

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_layer_bitmap**
> Layer post_layer_bitmap(layer_bitmap_form)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.LayerBitmapControllerApi(api_client)
    layer_bitmap_form = cartotool.LayerBitmapForm() # LayerBitmapForm | 

    try:
        api_response = api_instance.post_layer_bitmap(layer_bitmap_form)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LayerBitmapControllerApi->post_layer_bitmap: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **layer_bitmap_form** | [**LayerBitmapForm**](LayerBitmapForm.md)|  | 

### Return type

[**Layer**](Layer.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_layer_bitmap**
> Layer put_layer_bitmap(code, layer_bitmap_form)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.LayerBitmapControllerApi(api_client)
    code = 'code_example' # str | 
layer_bitmap_form = cartotool.LayerBitmapForm() # LayerBitmapForm | 

    try:
        api_response = api_instance.put_layer_bitmap(code, layer_bitmap_form)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LayerBitmapControllerApi->put_layer_bitmap: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 
 **layer_bitmap_form** | [**LayerBitmapForm**](LayerBitmapForm.md)|  | 

### Return type

[**Layer**](Layer.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_layer_bitmap_chunk**
> object upload_layer_bitmap_chunk(code, index, inline_object=inline_object)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.LayerBitmapControllerApi(api_client)
    code = 'code_example' # str | 
index = 56 # int | 
inline_object = cartotool.InlineObject() # InlineObject |  (optional)

    try:
        api_response = api_instance.upload_layer_bitmap_chunk(code, index, inline_object=inline_object)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LayerBitmapControllerApi->upload_layer_bitmap_chunk: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 
 **index** | **int**|  | 
 **inline_object** | [**InlineObject**](InlineObject.md)|  | [optional] 

### Return type

**object**

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_layer_bitmap_complete**
> LayerBitmap upload_layer_bitmap_complete(code)



### Example

* Api Key Authentication (ApiKeyAuth):
```python
from __future__ import print_function
import time
import cartotool
from cartotool.rest import ApiException
from pprint import pprint
configuration = cartotool.Configuration()
# Configure API key authorization: ApiKeyAuth
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Defining host is optional and default to http://cartotool.com
configuration.host = "http://cartotool.com"

# Enter a context with an instance of the API client
with cartotool.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cartotool.LayerBitmapControllerApi(api_client)
    code = 'code_example' # str | 

    try:
        api_response = api_instance.upload_layer_bitmap_complete(code)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LayerBitmapControllerApi->upload_layer_bitmap_complete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

[**LayerBitmap**](LayerBitmap.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

