# Install new api docs

Download swagger.json from http://localhost:8080/api-docs
Format code using your IDE

Add the Authentication configuration at the bottom of the swagger file.
```json
 "securitySchemes": {
      "ApiKeyAuth": {
        "type": "apiKey",
        "in": "header",
        "name": "Authorization"
      }
    }
  },
  "security": [
    {
      "ApiKeyAuth": []
    }
  ]
```

Set the new version in `config.json`

Run in terminal
`openapi-generator generate -i swagger.json -g python -c config.json`

Check that README.md has been updated

Commit and done!

# Release guide
https://packaging.python.org/tutorials/packaging-projects/

## Release manually to test pypi

`python3 -m pip install --user --upgrade setuptools wheel`

`python3 setup.py sdist bdist_wheel`

`python3 -m pip install --user --upgrade twine`

`python3 -m twine upload --repository testpypi dist/* -u ${PYPI_USERNAME} -p ${PYPI_PASSWORD}`

## Release to production pypi (pipeline should do this step)

`python3 -m pip install --user --upgrade setuptools wheel`

`python3 setup.py sdist bdist_wheel`

`python3 -m pip install --user --upgrade twine`

`python3 -m twine upload dist/* -u ${PYPI_USERNAME} -p ${PYPI_PASSWORD}`
