# coding: utf-8

"""
    Cartotool

    Cartotool api project for using the cartotool.com application with Python scripts  # noqa: E501

    The version of the OpenAPI document: 1.0.4
    Contact: info@cartotool.com
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from cartotool.configuration import Configuration


class LayerGroup(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'id': 'int',
        'name': 'str',
        'parent': 'LayerGroup',
        'position': 'int',
        'project': 'Project'
    }

    attribute_map = {
        'id': 'id',
        'name': 'name',
        'parent': 'parent',
        'position': 'position',
        'project': 'project'
    }

    def __init__(self, id=None, name=None, parent=None, position=None, project=None, local_vars_configuration=None):  # noqa: E501
        """LayerGroup - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._id = None
        self._name = None
        self._parent = None
        self._position = None
        self._project = None
        self.discriminator = None

        if id is not None:
            self.id = id
        if name is not None:
            self.name = name
        if parent is not None:
            self.parent = parent
        if position is not None:
            self.position = position
        if project is not None:
            self.project = project

    @property
    def id(self):
        """Gets the id of this LayerGroup.  # noqa: E501


        :return: The id of this LayerGroup.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this LayerGroup.


        :param id: The id of this LayerGroup.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def name(self):
        """Gets the name of this LayerGroup.  # noqa: E501


        :return: The name of this LayerGroup.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this LayerGroup.


        :param name: The name of this LayerGroup.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def parent(self):
        """Gets the parent of this LayerGroup.  # noqa: E501


        :return: The parent of this LayerGroup.  # noqa: E501
        :rtype: LayerGroup
        """
        return self._parent

    @parent.setter
    def parent(self, parent):
        """Sets the parent of this LayerGroup.


        :param parent: The parent of this LayerGroup.  # noqa: E501
        :type: LayerGroup
        """

        self._parent = parent

    @property
    def position(self):
        """Gets the position of this LayerGroup.  # noqa: E501


        :return: The position of this LayerGroup.  # noqa: E501
        :rtype: int
        """
        return self._position

    @position.setter
    def position(self, position):
        """Sets the position of this LayerGroup.


        :param position: The position of this LayerGroup.  # noqa: E501
        :type: int
        """

        self._position = position

    @property
    def project(self):
        """Gets the project of this LayerGroup.  # noqa: E501


        :return: The project of this LayerGroup.  # noqa: E501
        :rtype: Project
        """
        return self._project

    @project.setter
    def project(self, project):
        """Sets the project of this LayerGroup.


        :param project: The project of this LayerGroup.  # noqa: E501
        :type: Project
        """

        self._project = project

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, LayerGroup):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, LayerGroup):
            return True

        return self.to_dict() != other.to_dict()
