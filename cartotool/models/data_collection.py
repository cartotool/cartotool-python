# coding: utf-8

"""
    Cartotool

    Cartotool api project for using the cartotool.com application with Python scripts  # noqa: E501

    The version of the OpenAPI document: 1.0.4
    Contact: info@cartotool.com
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from cartotool.configuration import Configuration


class DataCollection(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'id': 'int',
        'code': 'str',
        'project': 'Project',
        'name': 'str',
        'description': 'str',
        'size': 'int',
        'head': 'list[str]',
        'geometry': 'DataCollectionGeometry',
        'status': 'str',
        'creation_date': 'datetime'
    }

    attribute_map = {
        'id': 'id',
        'code': 'code',
        'project': 'project',
        'name': 'name',
        'description': 'description',
        'size': 'size',
        'head': 'head',
        'geometry': 'geometry',
        'status': 'status',
        'creation_date': 'creationDate'
    }

    def __init__(self, id=None, code=None, project=None, name=None, description=None, size=None, head=None, geometry=None, status=None, creation_date=None, local_vars_configuration=None):  # noqa: E501
        """DataCollection - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._id = None
        self._code = None
        self._project = None
        self._name = None
        self._description = None
        self._size = None
        self._head = None
        self._geometry = None
        self._status = None
        self._creation_date = None
        self.discriminator = None

        if id is not None:
            self.id = id
        if code is not None:
            self.code = code
        if project is not None:
            self.project = project
        if name is not None:
            self.name = name
        if description is not None:
            self.description = description
        if size is not None:
            self.size = size
        if head is not None:
            self.head = head
        if geometry is not None:
            self.geometry = geometry
        if status is not None:
            self.status = status
        if creation_date is not None:
            self.creation_date = creation_date

    @property
    def id(self):
        """Gets the id of this DataCollection.  # noqa: E501


        :return: The id of this DataCollection.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this DataCollection.


        :param id: The id of this DataCollection.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def code(self):
        """Gets the code of this DataCollection.  # noqa: E501


        :return: The code of this DataCollection.  # noqa: E501
        :rtype: str
        """
        return self._code

    @code.setter
    def code(self, code):
        """Sets the code of this DataCollection.


        :param code: The code of this DataCollection.  # noqa: E501
        :type: str
        """

        self._code = code

    @property
    def project(self):
        """Gets the project of this DataCollection.  # noqa: E501


        :return: The project of this DataCollection.  # noqa: E501
        :rtype: Project
        """
        return self._project

    @project.setter
    def project(self, project):
        """Sets the project of this DataCollection.


        :param project: The project of this DataCollection.  # noqa: E501
        :type: Project
        """

        self._project = project

    @property
    def name(self):
        """Gets the name of this DataCollection.  # noqa: E501


        :return: The name of this DataCollection.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this DataCollection.


        :param name: The name of this DataCollection.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def description(self):
        """Gets the description of this DataCollection.  # noqa: E501


        :return: The description of this DataCollection.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this DataCollection.


        :param description: The description of this DataCollection.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def size(self):
        """Gets the size of this DataCollection.  # noqa: E501


        :return: The size of this DataCollection.  # noqa: E501
        :rtype: int
        """
        return self._size

    @size.setter
    def size(self, size):
        """Sets the size of this DataCollection.


        :param size: The size of this DataCollection.  # noqa: E501
        :type: int
        """

        self._size = size

    @property
    def head(self):
        """Gets the head of this DataCollection.  # noqa: E501


        :return: The head of this DataCollection.  # noqa: E501
        :rtype: list[str]
        """
        return self._head

    @head.setter
    def head(self, head):
        """Sets the head of this DataCollection.


        :param head: The head of this DataCollection.  # noqa: E501
        :type: list[str]
        """

        self._head = head

    @property
    def geometry(self):
        """Gets the geometry of this DataCollection.  # noqa: E501


        :return: The geometry of this DataCollection.  # noqa: E501
        :rtype: DataCollectionGeometry
        """
        return self._geometry

    @geometry.setter
    def geometry(self, geometry):
        """Sets the geometry of this DataCollection.


        :param geometry: The geometry of this DataCollection.  # noqa: E501
        :type: DataCollectionGeometry
        """

        self._geometry = geometry

    @property
    def status(self):
        """Gets the status of this DataCollection.  # noqa: E501


        :return: The status of this DataCollection.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this DataCollection.


        :param status: The status of this DataCollection.  # noqa: E501
        :type: str
        """
        allowed_values = ["NEW", "UPLOADING", "COMPLETE", "ERROR", "DELETED"]  # noqa: E501
        if self.local_vars_configuration.client_side_validation and status not in allowed_values:  # noqa: E501
            raise ValueError(
                "Invalid value for `status` ({0}), must be one of {1}"  # noqa: E501
                .format(status, allowed_values)
            )

        self._status = status

    @property
    def creation_date(self):
        """Gets the creation_date of this DataCollection.  # noqa: E501


        :return: The creation_date of this DataCollection.  # noqa: E501
        :rtype: datetime
        """
        return self._creation_date

    @creation_date.setter
    def creation_date(self, creation_date):
        """Sets the creation_date of this DataCollection.


        :param creation_date: The creation_date of this DataCollection.  # noqa: E501
        :type: datetime
        """

        self._creation_date = creation_date

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, DataCollection):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, DataCollection):
            return True

        return self.to_dict() != other.to_dict()
