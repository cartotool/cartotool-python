# coding: utf-8

"""
    OpenAPI definition

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import openapi_client
from openapi_client.api.workspace_controller_api import WorkspaceControllerApi  # noqa: E501
from openapi_client.rest import ApiException


class TestWorkspaceControllerApi(unittest.TestCase):
    """WorkspaceControllerApi unit test stubs"""

    def setUp(self):
        self.api = openapi_client.api.workspace_controller_api.WorkspaceControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_delete3(self):
        """Test case for delete3

        """
        pass

    def test_delete_user(self):
        """Test case for delete_user

        """
        pass

    def test_get(self):
        """Test case for get

        """
        pass

    def test_get_all(self):
        """Test case for get_all

        """
        pass

    def test_get_projects(self):
        """Test case for get_projects

        """
        pass

    def test_get_users(self):
        """Test case for get_users

        """
        pass

    def test_post7(self):
        """Test case for post7

        """
        pass

    def test_post_user(self):
        """Test case for post_user

        """
        pass


if __name__ == '__main__':
    unittest.main()
