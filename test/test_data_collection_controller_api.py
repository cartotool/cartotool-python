# coding: utf-8

"""
    OpenAPI definition

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import openapi_client
from openapi_client.api.data_collection_controller_api import DataCollectionControllerApi  # noqa: E501
from openapi_client.rest import ApiException


class TestDataCollectionControllerApi(unittest.TestCase):
    """DataCollectionControllerApi unit test stubs"""

    def setUp(self):
        self.api = openapi_client.api.data_collection_controller_api.DataCollectionControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_create_collection(self):
        """Test case for create_collection

        """
        pass

    def test_delete_by_code(self):
        """Test case for delete_by_code

        """
        pass

    def test_find_all1(self):
        """Test case for find_all1

        """
        pass

    def test_find_by_code(self):
        """Test case for find_by_code

        """
        pass

    def test_find_data_collection_by_code(self):
        """Test case for find_data_collection_by_code

        """
        pass


if __name__ == '__main__':
    unittest.main()
