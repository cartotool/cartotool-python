# coding: utf-8

"""
    OpenAPI definition

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import openapi_client
from openapi_client.api.map_controller_api import MapControllerApi  # noqa: E501
from openapi_client.rest import ApiException


class TestMapControllerApi(unittest.TestCase):
    """MapControllerApi unit test stubs"""

    def setUp(self):
        self.api = openapi_client.api.map_controller_api.MapControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_find_all3(self):
        """Test case for find_all3

        """
        pass

    def test_find_by_code1(self):
        """Test case for find_by_code1

        """
        pass


if __name__ == '__main__':
    unittest.main()
