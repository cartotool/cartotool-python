# coding: utf-8

"""
    OpenAPI definition

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import openapi_client
from openapi_client.api.layer_tms_controller_api import LayerTmsControllerApi  # noqa: E501
from openapi_client.rest import ApiException


class TestLayerTmsControllerApi(unittest.TestCase):
    """LayerTmsControllerApi unit test stubs"""

    def setUp(self):
        self.api = openapi_client.api.layer_tms_controller_api.LayerTmsControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_post2(self):
        """Test case for post2

        """
        pass

    def test_put2(self):
        """Test case for put2

        """
        pass


if __name__ == '__main__':
    unittest.main()
