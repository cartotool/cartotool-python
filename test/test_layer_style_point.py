# coding: utf-8

"""
    OpenAPI definition

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import openapi_client
from openapi_client.models.layer_style_point import LayerStylePoint  # noqa: E501
from openapi_client.rest import ApiException

class TestLayerStylePoint(unittest.TestCase):
    """LayerStylePoint unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test LayerStylePoint
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # model = openapi_client.models.layer_style_point.LayerStylePoint()  # noqa: E501
        if include_optional :
            return LayerStylePoint(
                id = 56, 
                value = '0', 
                label = '0', 
                position = 56, 
                fill_color = '0', 
                stroke_color = '0', 
                stroke_width = 1.337, 
                size = 1.337, 
                icon = '0'
            )
        else :
            return LayerStylePoint(
        )

    def testLayerStylePoint(self):
        """Test LayerStylePoint"""
        inst_req_only = self.make_instance(include_optional=False)
        inst_req_and_optional = self.make_instance(include_optional=True)


if __name__ == '__main__':
    unittest.main()
