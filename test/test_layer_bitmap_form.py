# coding: utf-8

"""
    OpenAPI definition

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import openapi_client
from openapi_client.models.layer_bitmap_form import LayerBitmapForm  # noqa: E501
from openapi_client.rest import ApiException

class TestLayerBitmapForm(unittest.TestCase):
    """LayerBitmapForm unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test LayerBitmapForm
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # model = openapi_client.models.layer_bitmap_form.LayerBitmapForm()  # noqa: E501
        if include_optional :
            return LayerBitmapForm(
                name = '0', 
                description = '0', 
                footnote = '0', 
                group = 56, 
                position = 56, 
                project = '0', 
                polygon_styles = [
                    openapi_client.models.layer_style_polygon.LayerStylePolygon(
                        id = 56, 
                        value = '0', 
                        label = '0', 
                        position = 56, 
                        fill_color = '0', 
                        stroke_color = '0', 
                        stroke_width = 1.337, 
                        stroke_line_dash = 'SOLID', )
                    ], 
                point_styles = [
                    openapi_client.models.layer_style_point.LayerStylePoint(
                        id = 56, 
                        value = '0', 
                        label = '0', 
                        position = 56, 
                        fill_color = '0', 
                        stroke_color = '0', 
                        stroke_width = 1.337, 
                        size = 1.337, 
                        icon = '0', )
                    ], 
                legend_enabled = True, 
                hidden_layer = True, 
                visible = True, 
                opacity = 1.337, 
                base_layer = True
            )
        else :
            return LayerBitmapForm(
        )

    def testLayerBitmapForm(self):
        """Test LayerBitmapForm"""
        inst_req_only = self.make_instance(include_optional=False)
        inst_req_and_optional = self.make_instance(include_optional=True)


if __name__ == '__main__':
    unittest.main()
